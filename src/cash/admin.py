# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import User

class UserAdmin(admin.ModelAdmin):
    fields = ('username', 'email', 'currency')
    list_display = ('username',)
    search_fields = ('username',)
    readonly_fields = ('username',)
    
admin.site.register(User, UserAdmin)
