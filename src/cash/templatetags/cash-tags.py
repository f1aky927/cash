# -*- coding: utf-8 -*-

from django import template
import datetime, time, math, re
 
register = template.Library()

@register.filter(name= 'get_date')
def get_date(value):
    return value[0].date


@register.filter(name= 'month_balance')
def month_balance(value):
    balance = sum([t.value for t in value]) or 0
    if balance >= 0:
        css_class = 'text-success'
        prefix = '+'
    else:
        css_class = 'text-error'
        prefix = ''
    sent = balance - int(balance)
    if sent == 0:
        balance_display = int(balance)
    else: 
        balance_display = '%.2f' % balance
    return '<span class="%s">%s%s</span>' % (css_class, prefix, balance_display)     

    