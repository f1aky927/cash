# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

CURRENCY_CHOICES =(
    ('ru', _(u'руб.')),
    ('ua', _(u'грн.'))
)

class User(AbstractUser):
    
    currency = models.CharField(_(u'валюта'), max_length= 10, choices= CURRENCY_CHOICES, default= 'ru')
    balans = models.FloatField(_(u'баланс'), default= 0)

    def get_balans_display(self):
        if self.balans >= 0: prefix = '+'
        else: prefix = ''
        sent = self.balans - int(self.balans)
        if sent == 0: display = int(self.balans)
        else: display = '%.2f' % self.balans
        return '%s%s' % (prefix, display) 

    def get_balans_css(self):
        if self.balans >= 0:
            return 'success'
        return 'error'
           


TRANSACTION_KIND = (
    ('income', _(u'даход')),
    ('expense', _(u'расход')),
)
    
#транзакция
class Transaction(models.Model):

    user = models.ForeignKey('User', related_name= 'transactions', verbose_name= _(u'пользователь'))
    value = models.FloatField(_(u'сумма'), help_text= _(u'например 120 или -200'))
    kind = models.CharField(_(u'вид'), max_length= 30, choices= TRANSACTION_KIND)
    date = models.DateField(_(u'дата'), auto_now_add= False)
    note = models.TextField(_(u'примечание'), null= True, blank= True)
    
    def __unicode__(self):
        return '%s %s %s' % (self.user, self.get_value_display(), self.date)

    class Meta:
        verbose_name = _(u'транзакции')
        verbose_name_plural = _(u'транзакции') 
        ordering = ('user', '-date', '-id') 

    def save(self, *args, **kwargs):
        if self.value > 0:
            self.kind = 'income'
        else:
            self.kind = 'expense'
        return super(Transaction, self).save(*args, **kwargs)
  
    def get_value_display(self):
        if self.value >= 0: prefix = '+'
        else: prefix = ''
        sent = self.value - int(self.value)
        if sent == 0: display = int(self.value)
        else: display = '%.2f' % self.value
        return '%s%s' % (prefix, display) 

    def get_value_css(self):
        if self.value >= 0:
            return 'success'
        return 'error'

    def get_tags_display(self):
        return ', '.join([tag.title for tag in self.tags.all()])
 
class Tag(models.Model):

    title = models.CharField(_(u'заголовок'), max_length= 80, unique= True)
    transactions = models.ManyToManyField('Transaction', related_name= 'tags', verbose_name= _(u'транзакции'), null= True, blank= True) 
           
    def __unicode__(self):
        return u'%s' % self.title.lower()  
        
    class Meta:
        verbose_name = _(u'тег')
        verbose_name_plural = _(u'теги') 
     
        
        
        