# -*- coding: utf-8 -*-

from ..decorators import render_to, render_to_json
from django.shortcuts import redirect, get_object_or_404
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponse
from django.db.models import Count, Sum, Q
from .models import Transaction, Tag
from .forms import AuthenticationForm, RegistrationForm, TransactionForm
from dateutil.relativedelta import relativedelta
import datetime, itertools, calendar, simplejson as json

User = get_user_model()


@render_to('mainpage.html')
def mainpage(request):
    form = AuthenticationForm(request.POST or None)
    if form.is_valid():
        auth.login(request, form._user)
        return redirect('cash:cash')
    return dict(
        form= form,
    )


@render_to('registration.html')
def registration(request):
    form = RegistrationForm(request.POST or None)
    if form.is_valid():
        form.save()
        cd = form.cleaned_data
        user = auth.authenticate(username= cd['username'], password=cd['password1'])
        auth.login(request, user)
        return redirect('cash:cash')
    return dict(
        form= form,
    )


def logout(request):
    if request.user.is_authenticated(): 
        auth.logout(request)
    return redirect('mainpage')

@login_required
@render_to('cash/cash.html')
def cash(request):
    form = TransactionForm(user= request.user)     
    now = datetime.datetime.now()
    last = now - relativedelta(months=1)
    return dict(
        form= form,
        now_date = ('%02d' % now.month, now.year),
        last_date = ('%02d' % last.month, last.year),
    )

def transaction_add(request): 
    form = TransactionForm(data= request.GET or None, user= request.user)    
    if request.is_ajax() and form.is_valid():
        obj = form.save()
        d = dict(
            url= '%02d/%04d/' % (obj.date.month, obj.date.year),
            balans_css= request.user.get_balans_css(),
            balans= request.user.get_balans_display(),
        )
        return HttpResponse(json.dumps(d), mimetype= "application/json")
    print form.errors
    raise Http404()


def transaction_edit(request):
    if not request.GET.get('id'):
        raise Http404()
    transaction = get_object_or_404(Transaction, id= request.GET.get('id'), user= request.user)
    value = transaction.value 
    form = TransactionForm(data= request.GET or None, user= request.user, instance= transaction)    
    if request.is_ajax() and form.is_valid():
        transaction.tags.clear()
        request.user.balans -= value # юзер сохраняеться в форме
        obj = form.save()
        d = dict(
            url= '%02d/%04d/' % (obj.date.month, obj.date.year),
            balans_css= request.user.get_balans_css(),
            balans= request.user.get_balans_display(),
        )
        return HttpResponse(json.dumps(d), mimetype= "application/json")
    raise Http404()        


def transaction_del(request):
    try:
        transaction = get_object_or_404(Transaction, id= request.GET.get('id'), user= request.user)
    except ValueError:
        raise Http404()
    request.user.balans -= transaction.value 
    d = dict(
        url= '%02d/%04d/' % (transaction.date.month, transaction.date.year),
        balans_css= request.user.get_balans_css(),
        balans= request.user.get_balans_display(),
    )
    transaction.delete()    
    return HttpResponse(json.dumps(d), mimetype= "application/json")
 
    
@render_to('cash/transaction_month.html')
def transaction(request, month, year):
    q_list = [Q(user= request.user), Q(date__month= month), Q(date__year= year)]
    if request.GET.has_key('tag'):
        tag= request.GET.get('tag')
        if tag != '_all':    
            q_list.append(Q(tags__title= tag))
    transactions = Transaction.objects.filter(*q_list).prefetch_related('tags')      
    return dict(
        transactions= transactions,
    )
                                              

@render_to('cash/transaction_all.html')
def transaction_all(request):
    q_list = [Q(user= request.user)]
    if request.GET.has_key('tag'):
        tag= request.GET.get('tag')
        if tag != '_all':    
            q_list.append(Q(tags__title= tag))
    ts = Transaction.objects.filter(*q_list).all().prefetch_related('tags')
    t_group = dict()
    for k, v in itertools.groupby(ts, lambda obj: '%04d%02d' % (obj.date.year, obj.date.month)):
        t_group[k] = list(v)
    keys = t_group.keys()
    keys.sort()
    keys.reverse()
    transactions = [t_group[key] for key in keys]
    return dict(
        transactions= transactions,
    )

@render_to('cash/tags_list.html')
def tag(request, month, year):
    tags_sum = Transaction.objects.filter(user= request.user, date__month= month, date__year= year).count()    
    tags = Tag.objects.filter(transactions__user= request.user, transactions__date__month= month, transactions__date__year= year).annotate(t_count= Count('transactions'))
    return dict(
        tags= tags,
        tags_sum= tags_sum 
    )
                                              

@render_to('cash/tags_list.html')
def tag_all(request):
    tags_sum = Transaction.objects.filter(user= request.user).count()    
    tags = Tag.objects.filter(transactions__user= request.user).all().annotate(t_count= Count('transactions'))
    return dict(
        tags= tags,
        tags_sum= tags_sum
    )


def chart(request, month, year):
    q_list = [Q(user= request.user), Q(date__month= month), Q(date__year= year)]
    t_all = Transaction.objects.filter(*q_list).all()      
    tags_days = list()
    if request.GET.has_key('tag'):
        tag= request.GET.get('tag')
        if tag != '_all':    
            q_list.append(Q(tags__title= tag))
            tags_days = set(['%02d' % t['date'].day for t in Transaction.objects.filter(*q_list).values('date')])            
    t_group = dict()
    for k, v in itertools.groupby(t_all, lambda obj: '%02d' % (obj.date.day)):
        t_group[k] = list(v)
    data = list()
    days = calendar.monthrange(int(year), int(month))[1]
    month_balans = 0
    this_emphasis = False
    next_emphasis = False
    for _day in xrange(1, days+1):
        day = '%02d' % _day
        t_day = t_group.get(day, list())
        month_balans += sum([t.value for t in t_day])    
        if next_emphasis:
            this_emphasis = True
            next_emphasis = False
        if day in tags_days:
            this_emphasis = True
            next_emphasis = True
        data.append([day, month_balans, this_emphasis])
        this_emphasis = False
    return HttpResponse(json.dumps(data), mimetype= "application/json")
    
        
    